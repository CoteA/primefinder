# PrimeFinder

About PrimeFinder
==========
This is a class to find prime numbers and evaluate them. It is semi-optimized to only divide by prime numbers. It auto-search for new prime number if needed and doesn't research for prime who were evaluated before.

This project is a simple algorithm (more complex that FizzBuzz) to show the kind of code I write.
The code could be more pythonic, but I want it to be readable by those not used to python's way.

Requirements
------------
* Python 3.4

Links
-----
Gitlab:     https://gitlab.com/CoteAnthony/primefinder
