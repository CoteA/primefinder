#!/usr/bin/env python
# File: test_PrimeFinder.py
# By  : Anthony Cote
# Date: 2018-09-08
# Goal: Teste la classe primeFinder

from unittest import TestCase

from PrimeFinder import PrimeFinder


class TestPrimeFinder(TestCase, PrimeFinder):
    
    @classmethod
    def setUp(cls):
        cls.reset(cls)
    
    @classmethod
    def tearDown(cls):
        pass
    
    def reset(self):
        self.prime_list = [2, 3, 5, 7, 11]
        self.highest_evaluated_value = self.prime_list[-1]
    
    def test_increase_list(self):
        assert 11 == self.last_prime_found()
        self.increase_list()
        assert 11 == self.last_prime_found()
        self.increase_list()
        assert 13 == self.last_prime_found()
        self.increase_list()
        assert 13 == self.last_prime_found()
        self.increase_list()
        assert 13 == self.last_prime_found()
    
    def test_fill_list_up_to(self):
        self.fill_list_up_to(20)
        assert [2, 3, 5, 7, 11, 13, 17, 19] == self.prime_list
    
    def test_is_prime(self):
        assert self.is_prime(1) is False
        assert self.is_prime(2)
        assert self.is_prime(3)
        assert self.is_prime(4) is False
        assert self.is_prime(5)
        assert self.is_prime(6) is False
        assert self.is_prime(7)
        assert self.is_prime(8) is False
        assert self.is_prime(9) is False
        assert self.is_prime(10) is False
        assert self.is_prime(11)
        assert self.is_prime(12) is False
        assert self.is_prime(13)
        assert self.is_prime(14) is False
        assert self.is_prime(15) is False
        assert self.is_prime(16) is False
        assert self.is_prime(17)
    
    def test_last_prime_found(self):
        assert 11 == self.last_prime_found()
        self.generate_new_prime()
        assert 13 == self.last_prime_found()
        self.generate_new_prime()
        assert 17 == self.last_prime_found()
    
    def test_index_inferior(self):
        assert self.index_inferior(0) is None
        assert self.index_inferior(1) is None
        assert 0 == self.index_inferior(2)
        assert 1 == self.index_inferior(3)
        assert 1 == self.index_inferior(4)
        assert 2 == self.index_inferior(5)
        assert 2 == self.index_inferior(6)
        assert 3 == self.index_inferior(7)
        assert 3 == self.index_inferior(8)
        assert 3 == self.index_inferior(9)
    
    def test_index_superior(self):
        assert 0 == self.index_superior(0)
        assert 0 == self.index_superior(1)
        assert 0 == self.index_superior(2)
        assert 1 == self.index_superior(3)
        assert 2 == self.index_superior(4)
        assert 2 == self.index_superior(5)
        assert 3 == self.index_superior(6)
        assert 3 == self.index_superior(7)
        assert 4 == self.index_superior(8)
        assert 4 == self.index_superior(9)
    
    def test_generate_new_prime(self):
        assert 13 == self.generate_new_prime()
        assert 17 == self.generate_new_prime()
        assert 19 == self.generate_new_prime()
        assert 23 == self.generate_new_prime()
    
    def test_prime_by_index(self):
        assert self.prime_by_index(-1) is None
        assert 2 == self.prime_by_index(0)
        assert 3 == self.prime_by_index(1)
        assert 5 == self.prime_by_index(2)
        assert 7 == self.prime_by_index(3)
        
        assert 13 == self.prime_by_index(5)
        assert 17 == self.prime_by_index(6)
    
    def test_closest_primes(self):
        assert (None, 2) == self.closest_primes(1)
        assert (None, 3, 2) == self.closest_primes(2)
        
        assert (3, 7, 5) == self.closest_primes(5)
        assert (11, 17, 13) == self.closest_primes(13)
        
        assert (3, 5) == self.closest_primes(4)
        assert (13, 17) == self.closest_primes(15)
    
    def test_is_not_divisible(self):
        assert self.is_not_divisible(10, 5) is False
        assert self.is_not_divisible(10, 10)
        assert self.is_not_divisible(7, 5)
        assert self.is_not_divisible(1, 5)
    
    def test_divisible_and_different(self):
        assert self.is_divisible_and_different(10, 5)
        assert self.is_divisible_and_different(10, 10) is False
        assert self.is_divisible_and_different(7, 5) is False
        assert self.is_divisible_and_different(1, 5) is False
    
    def test_is_in_prime_list(self):
        assert self.is_in_prime_list(1) is False
        assert self.is_in_prime_list(2)
        assert self.is_in_prime_list(3)
        assert self.is_in_prime_list(4) is False
        assert self.is_in_prime_list(5)
        assert self.is_in_prime_list(6) is False
        assert self.is_in_prime_list(7)
        assert self.is_in_prime_list(8) is False
        assert self.is_in_prime_list(9) is False
        assert self.is_in_prime_list(10) is False
        assert self.is_in_prime_list(11)
        assert self.is_in_prime_list(12) is False
        assert self.is_in_prime_list(13)
        assert self.is_in_prime_list(14) is False
        assert self.is_in_prime_list(15) is False

    def test_next(self):
        self.reset_generator()
        assert 2 == self.next()
        assert 3 == self.next()
        assert 5 == self.next()
        assert 7 == self.next()
        assert 11 == self.next()
        assert 13 == self.next()
        assert 17 == self.next()
        assert 19 == self.next()
        self.reset_generator()
        assert 2 == self.next()
        assert 3 == self.next()
        assert 5 == self.next()
        assert 7 == self.next()
        assert 11 == self.next()
        assert 13 == self.next()
        assert 17 == self.next()
        assert 19 == self.next()

