#!/usr/bin/env python
# File: PrimeFinder.py
# By  : Anthony Cote
# Date: 2017-09-24
# Goal: Manage prime numbers

from typing import Optional
from typing import Union


# This code is designed for showing my natural code style
# It balance performance et readability so it doesn't excel at any
class PrimeFinder:

    # Init
    prime_list = [2, 3, 5, 7, 11]
    highest_evaluated_value = prime_list[-1]
    generator_index = -1
        
    # Essaye les nombres suivants pour voir s'ils sont prime
    def increase_list(self, by_how_much: int =1) -> None:
        for _ in range(by_how_much):
            self.highest_evaluated_value += 1
            if self.is_prime(self.highest_evaluated_value):
                self.prime_list.append(self.highest_evaluated_value)

    # Complete la liste jusqu'au niveau maximum chercher
    def fill_list_up_to(self, new_target: int) -> None:
        how_many = new_target - self.highest_evaluated_value
        if how_many > 0:
            self.increase_list(how_many)

    # retourne si ce n'est pas divisible par le nombre premier
    def is_not_divisible(self, evaluated_value: int, each_prime: int) -> bool:
        return (evaluated_value % each_prime) != 0 or evaluated_value == each_prime
    
    # retourne si c'est divisible et different de la valeur
    def is_divisible_and_different(self, evaluated_value: int, each_prime: int) -> bool:
        return (evaluated_value % each_prime) == 0 and evaluated_value != each_prime
    
    # Verifi si le nombre est premier
    def is_prime(self, evaluated_value: int) -> bool:
        # Rempli la liste jusqu'a la valeur chercher
        if evaluated_value > self.highest_evaluated_value:
            self.fill_list_up_to(evaluated_value)
        
        if evaluated_value < 2:
            return False
        if evaluated_value is 2:
            return True

        # Compare avec les nombre premiers connus
        return self.is_in_prime_list(evaluated_value)

    # Compare avec les nombres premiers connus
    def is_in_prime_list(self, evaluated_value: int, list: list =prime_list) -> bool:

        # Cherche la valeur dans la liste
        for each_prime in list:
            # Si seulement divisible par 1 et lui-meme (puisque c'est en ordre)
            if evaluated_value == each_prime:
                return True
            # Si divisible par le prime
            if (evaluated_value % each_prime) == 0:
                return False
            # Sort de la boucle si on a fini d'evaluer les prime inferieur a la valeur evaluer
            if evaluated_value < each_prime:
                return False
        return True  # Quand la valeur n'est pas trouver dans la liste de nombre prime
        
    # Retourne l'index le plus pres a la hausse
    def index_inferior(self, origin_value: int) -> Optional[int]:
        
        if origin_value <= 1:
            return None
        
        # Met a jour la liste jusqu'a la valeur d'origine
        if origin_value > self.highest_evaluated_value:
            self.fill_list_up_to(origin_value)
        
        # Cherche la premiere valeur superieur a la valeur chercher
        # et retourne la precedente
        for each_index in range(len(self.prime_list)):
            if self.prime_list[each_index] > origin_value:
                return each_index - 1
    
    # Retourne l'index le plus pres a la baisse
    def index_superior(self, origin_value: int) -> int:
        for each_index in range(len(self.prime_list)):
            if self.prime_list[each_index] >= origin_value:
                return each_index
    
    # Trouve un nouveau nombre prime
    def generate_new_prime(self) -> int:
        
        previous_length = len(self.prime_list)
        
        # compare size of the prime list until it increase
        while previous_length == len(self.prime_list):
            self.increase_list()
        return self.last_prime_found()
    
    # Retourne le dernier prime trouve
    def last_prime_found(self) -> int:
        return self.prime_list[-1]

    #  Retourne le prime a l'index donner
    def prime_by_index(self, index: int) -> Optional[int]:
        
        # Si invalid
        if index is None or index < 0:
            return None
        
        # Met a jour la liste de prime
        while index >= len(self.prime_list):
            self.generate_new_prime()
            
        return self.prime_list[index]

    # Retourne les nombre premiers les plus pres de celui donner en parametre
    def closest_primes(self, base_value: int) -> Union[tuple, int]:
        
        self.fill_list_up_to(base_value)
        self.generate_new_prime()
        
        # Si la valeur est deja prime
        if base_value <= 1:
            return None, 2
        if self.is_prime(base_value):
            inf = self.prime_by_index(self.index_inferior(base_value - 1))
            sup = self.prime_by_index(self.index_superior(base_value + 1))
            return inf, sup, base_value
        else:
            inf = self.prime_by_index(self.index_inferior(base_value))
            sup = self.prime_by_index(self.index_superior(base_value))
            return inf, sup
        
    # Generator de nombres premiers
    def generate(self):
        while True:
            self.generator_index += 1
            yield self.prime_by_index(self.generator_index)
            
    # Commence a 0 et retourne chaque prime tour par tour
    def next(self) -> int:
        return next(self.generate())
        
    # Signal au generator de recommencer a 0
    def reset_generator(self) -> None:
        self.generator_index = -1
